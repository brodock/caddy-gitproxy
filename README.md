# caddy-git-proxy

A Caddy plugin that enables git to synchronize a repository using the Smart HTTP protocol.

This plugin is still under development, so to test it with caddy, you need to patch caddy and
add it to the import list (see: `caddy/caddymain/run.go` in caddy source-code) .

Because it's not officialy released yet, I'm registering the configuration directive using the
`httpserver.RegisterDevDirective` function, but in the near future I will open a PR to make it
officialy available.

To test the plugin, you need to create  a Caddyfile similar to the example below:

```
localhost:8080 {
  root /path/to/repositories
  gitproxy /my-repo.git
  log stdout # if you want to see what's happening behind the scenes
}
```
