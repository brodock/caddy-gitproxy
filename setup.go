package gitproxy

import (
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func init() {
	caddy.RegisterPlugin("gitproxy", caddy.Plugin{
		ServerType: "http",
		Action:     setupGitProxy,
	})

	// add a non public directive until we are ready to release
	httpserver.RegisterDevDirective("gitproxy", "jwt")
}

func setupGitProxy(c *caddy.Controller) error {
	cfg := httpserver.GetConfig(c)
	gpr := GitProxyRouter{Paths: make(map[string]GitHandler), Root:	cfg.Root }
	gpr.Git = &GitAdapter{"/usr/bin/git"}

	for c.Next() { // skip the directive name
		if !c.NextArg() {
			return c.ArgErr()
		}
		filename := c.Val()
		// TODO: validate
		gpr.NewPath(filename)
	}

	// wire-up handler to a middleware
	mid := func(next httpserver.Handler) httpserver.Handler {
		gpr.Next = next
		return gpr
	}

	cfg.AddMiddleware(mid)

	return nil
}
