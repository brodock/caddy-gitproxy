package gitproxy

import (
	"io"
	"log"
	"os/exec"
	"strconv"
	"strings"
)

type GitAdapter struct {
	GitPath string
}

func (g GitAdapter) PacketWrite(str string) []byte {
	s := strconv.FormatInt(int64(len(str)+4), 16)

	if len(s)%4 != 0 {
		s = strings.Repeat("0", 4-len(s)%4) + s
	}

	return []byte(s + str)
}

func (g GitAdapter) PacketFinish() []byte {
	return []byte("0000")
}

func (g GitAdapter) PacketAdvertise(service string) []byte {
	return g.PacketWrite("# service=git-" + service + "\n")
}

func (g GitAdapter) Command(path string, args ...string) []byte {
	command := exec.Command(g.GitPath, args...)
	command.Dir = path
	out, err := command.Output()

	if err != nil {
		log.Print("Command: ", g.GitPath, args, "Path: ", path, "Output: ", out)
		log.Print(err)
	}

	return out
}

func (g GitAdapter) CommandRpc(path string, args ...string) (io.WriteCloser, io.ReadCloser, *exec.Cmd) {
	command := exec.Command(g.GitPath, args...)
	command.Dir = path

	// initializes stdin
	in, err := command.StdinPipe()
	if err != nil {
		log.Print(err)
	}

	// initializes stdout
	stdout, err := command.StdoutPipe()
	if err != nil {
		log.Print(err)
	}

	err = command.Start()
	if err != nil {
		log.Print(err)
	}

	return in, stdout, command
}
