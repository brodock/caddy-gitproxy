package gitproxy

import (
	"errors"
	"fmt"
	"github.com/mholt/caddy/caddyhttp/httpserver"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
)

type GitProxyRouter struct {
	Paths map[string]GitHandler
	Root  string
	Next  httpserver.Handler
	Git   *GitAdapter
}

type GitHandler struct {
	Repository string
	Routes     map[string]GitRoute
	Router     *GitProxyRouter
}

type GitRoute struct {
	Method  string
	Handler func(w http.ResponseWriter, r *http.Request) (int, error)
}

func (gpr GitProxyRouter) NewPath(path string) {
	handler := GitHandler{path, make(map[string]GitRoute), &gpr}
	handler.InitRoutes()
	gpr.Paths[path] = handler
}

func (gpr GitProxyRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) (int, error) {
	for path, handler := range gpr.Paths {
		if httpserver.Path(r.URL.Path).Matches(path) {
			return handler.routeGitRequest(w, r)
		}
	}

	return gpr.Next.ServeHTTP(w, r)
}

func (h GitHandler) InitRoutes() {
	h.addRoute(h.Repository+"/info/refs", GitRoute{"GET", h.handleInfoRefs})
	h.addRoute(h.Repository+"/git-upload-pack", GitRoute{"POST", h.handleUploadPack})
}

func (h GitHandler) addRoute(path string, route GitRoute) {
	h.Routes[path] = route
}

func (h GitHandler) git() *GitAdapter {
	return h.Router.Git
}

func (h GitHandler) repositoryFullPath() string {
	return filepath.Join(h.Router.Root, h.Repository)
}

// routeGitRequest process requested path and query params to the specific handlers for each supported git action
func (h GitHandler) routeGitRequest(w http.ResponseWriter, r *http.Request) (int, error) {
	currentPath := r.URL.Path

	route, found := h.Routes[currentPath]
	if found && route.Method == r.Method {
		log.Println("Git Proxy:", r.Method, r.URL.Path)
		return route.Handler(w, r)
	} else {
		return http.StatusNotFound, errors.New(fmt.Sprintf("Invalid Git Proxy request: %s %s", r.Method, currentPath))
	}
}

func (h GitHandler) handleInfoRefs(w http.ResponseWriter, r *http.Request) (int, error) {
	// We should handle this request only if it's Smart HTTP
	serviceParam := r.URL.Query().Get("service")

	// We support only git-upload-pack service
	if serviceParam != "git-upload-pack" {
		return http.StatusForbidden, errors.New(fmt.Sprintf("Invalid service: %s", serviceParam))
	} else if serviceParam == "" {
		// Let's Caddy handle the request
		h.Router.Next.ServeHTTP(w, r)
	}

	// Advertise Smart HTTP support
	w.Header().Set("Content-Type", "application/x-git-upload-pack-advertisement")
	w.Header().Set("Cache-Control", "no-cache")

	// Advertise service packet
	w.Write(h.git().PacketAdvertise("upload-pack"))
	w.Write(h.git().PacketFinish())

	// Repository content
	args := []string{"upload-pack", "--stateless-rpc", "--advertise-refs", "."}
	w.Write(h.git().Command(h.repositoryFullPath(), args...))

	return 0, nil // signal to caddy we have written to the request
}

func (h GitHandler) handleUploadPack(w http.ResponseWriter, r *http.Request) (int, error) {
	contentType := r.Header.Get("Content-Type")

	// Lets validate the Content-Type
	if contentType == "" {
		return http.StatusBadRequest, errors.New(fmt.Sprintf("No content type specified"))
	} else if contentType != "application/x-git-upload-pack-request" {
		return http.StatusBadRequest, errors.New(fmt.Sprintf("Invalid content type specified: %s", contentType))
	}

	// git upload-pack Header
	w.Header().Set("Content-Type", "application/x-git-upload-pack-result")

	// Initializes RPC interface
	args := []string{"upload-pack", "--stateless-rpc", "."}
	in, out, command := h.git().CommandRpc(h.repositoryFullPath(), args...)

	// RPC Request
	input, _ := ioutil.ReadAll(r.Body)
	in.Write(input)
	in.Close()

	// RPC Response
	io.Copy(w, out)
	command.Wait()

	return 0, nil // signal to caddy we have written to the request
}
