package gitproxy

import (
	"testing"
	"github.com/mholt/caddy"
)

func TestIncompleteParams(t *testing.T) {
	c := caddy.NewTestController("http", "gitproxy")

	err := setupGitProxy(c)

	if err == nil {
		t.Error("Expected an error, but didn't get one")
	}
}

func TestSetup(t *testing.T) {
	c := caddy.NewTestController("http", "gitproxy repository.git")

	err := setupGitProxy(c)

	if err != nil {
		t.Errorf("Expected no errors, got: %v", err)
	}
}

